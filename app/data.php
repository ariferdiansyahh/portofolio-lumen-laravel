<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

use App\Status;

class Data extends Model
{
    protected $table = 'users' ;
    protected $fillable = ['name', 'email'];
    protected $appends =['currentStat'];

    public function statuses()
    {
        return $this->hasMany('App\status', 'user_id', 'id');
    }

    public function getCurrentStatAttribute()
    {
        return status::select('*')->where('user_id', $this->id)->orderBy("created_at", 'desc')->first();
        // return $this->hasMany('App\status', 'user_id', 'id');
    }

}