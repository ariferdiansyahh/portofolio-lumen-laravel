<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;
use App\User;
use App\Data;
use App\Website;
use App\Status;
class AuthController extends Controller
{
    
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }

    public function index(Request $request){
        
        $hasil = Data::all();

        return response()->json([
            
           'data' => $hasil ,
           
            ]);
    }

    public function status(Request $request){
        $hasil = Status::all();

        return response()->json([
            'data' => $hasil
        ]);
    }

    public function statusCreate(Request $request){
        //$hasil = status::all();
        $status_id = $request->input('status_id');
        $user_id = $request->input('user_id');

        $create = Status::create(['status_id' => $status_id, 'user_id' => $user_id]);
        return response()->json([
            'data' => $create
        ]);
    }
    
    public function join(Request $request){
     
        // $hasil = DB::table('users')->orderBy('created_at', 'desc')
        // ->join('user_statuses', 'user_statuses.user_id', '=' ,'users.id' )->distinct('user_statuses.created_at')
        // ->select('users.id','users.name','users.email','user_statuses.user_id','user_statuses.status_id', 'user_statuses.created_at')
        // ->get();
        $x = Data::select('id', 'name', 'email')->get();
        $hasil = json_decode($x); //cuman segini
        
        // $hasil = DB::table('user_statuses')->orderBy('created_at', 'desc')
        // ->join('users', 'users.id', '=' ,'user_id' )
        // ->select('users.id','users.name','users.email','user_statuses.user_id','user_statuses.status_id', 'user_statuses.created_at')
        // ->get();


        return response()->json([
            'data' => $hasil
        ]);
        
    }

    public function show($id){

        // header("Access-Control-Allow-Origin: *");
        $data = Data::where('id',$id)->get();
        return response ()->json([
            'data' => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        // header("Access-Control-Allow-Origin: *");
        $name = $request->input('name');
        $email = $request->input('email');
        $update = Data::where('id', $id)->update(['name' => $name, 'email' => $email]);
        if ($update){
            
            return response()->json([
                'status' => "updated"
            ]);
        }

    }

    public function balance(Request $request)
    {
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://mydomain.cloud.id/domainkuAPI/dapi.php",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "authemail=id.website%40website.id&token=UHNQqCgTF5ySYXdhj6L097Wr&action=GetCreditBalance&sld=a&tld=a&undefined=",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded"
            ),
            ));
            
            $response = curl_exec($curl);
            $json = json_decode($response, TRUE);
            $err = curl_error($curl);
            
            curl_close($curl);
            
            if ($err) {
            echo "cURL Error #:" . $err;
            } else {
            return response()->json([
                'data' => $json
                ]);
            }
    }

    public function website(Request $request)
    {
        $hasil = website::all();

        return response()->json([
            
           'data' => $hasil ,
           
            ]);
    }
    
    public function postLogin(Request $request)
    {   
       // header("Access-Control-Allow-Origin: *");
        $this->validate($request, [
            'email'    => 'required|email|max:255',
            'password' => 'required',
        ]);


        try {

            if (! $token = $this->jwt->attempt($request->only('email', 'password'))) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], 500);

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], 500);

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent' => $e->getMessage()], 500);

        }

        return response()->json([
         
         'status' => true,
        'token' => compact('token')
        ],200,compact('token')
        );
    }
}