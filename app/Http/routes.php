<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});
$app->get('/user', 'AuthController@index');
$app->get('/join', 'AuthController@join');
$app->get('/status', 'AuthController@status');
$app->post('/statusCreate', 'AuthController@statusCreate');
$app->get('/user/{id}', 'AuthController@show');
$app->put('/user/{id}', 'AuthController@update');
$app->get('/balance', 'AuthController@balance');
$app->get('/website', 'AuthController@website');
$app->post('/auth/login', 'AuthController@postLogin');

$app->group(['middleware' => 'auth:api'], function($app)
{
    $app->get('/test', function() {
        return response()->json([
            'message' => 'Hello World!',
        ]);
    });
});